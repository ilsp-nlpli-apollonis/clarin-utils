# Delete pod 
cfg=$1
ns=$2
container=$3

kubectl --kubeconfig=$cfg get pods -n $ns | grep $container | awk '{print $1}' | xargs kubectl --kubeconfig=$cfg delete pod -n $ns

# Watch until is up 
watch "kubectl --kubeconfig=$cfg get pods -n $ns | grep $container"
